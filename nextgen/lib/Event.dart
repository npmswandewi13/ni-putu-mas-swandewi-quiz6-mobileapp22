import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/rendering.dart';


class EventList extends StatefulWidget {
  const EventList({Key? key}) : super(key: key);

  @override
  _EventListState createState() => _EventListState();
} 

class _EventListState extends State<EventList> {
  @override
  
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(16),
      child: Column(
        children: [
          //Event 1
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Text(
                    'ICASTEM',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.red,
                      fontSize: 16,
                    ),
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                  ),
                  4.height,
                  const Text(
                    'Ayo ikuti ICASTEM 2022 : The 2nd International Conference on Applied Science, Technology, Engineering, and Mathematics',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.black,
                      fontSize: 16,
                    ),
                    maxLines: 2,
                    softWrap: true,
                    overflow: TextOverflow.ellipsis,
                  ),
                  10.height,

                  //Button Register
                  Container(
                    height: 40,
                    padding: const EdgeInsets.fromLTRB(0, 0, 10, 0),
                    child: ElevatedButton(
                      child: const Text('Register'),
                      style: ButtonStyle(
                          backgroundColor:
                              MaterialStateProperty.all(Colors.red)),
                      onPressed: () => {
                        showDiaglogBox(context),
                        NotificationService().showNotification(
                          1, 
                          'Registrasi Berhasil', 
                          'Kamu berhasil mendaftarkan diri dalam event : STEMPreneur Day', 
                          5)
                      },
                    ).cornerRadiusWithClipRRect(10),
                  ),
                  // End Button Register
                ],
              ).expand(flex: 2),
              4.width,
              Container(
                height: 100,
                color: Colors.grey,
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 2),
                  child: Image.asset(
                    'assets/image/Acara - ICASTEM.jpg',
                    fit: BoxFit.cover,
                    // height: 170,
                  ),
                ),
              ).cornerRadiusWithClipRRect(16).expand(flex: 1)
            ],
          ),
          //End Event 1

          18.height,
          //Event 2
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Text(
                    'Innofair 2022',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.red,
                      fontSize: 16,
                    ),
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                  ),
                  4.height,
                  const Text(
                    'Tempat untuk menunjukkan inovasi terbaikmu, kunjungi setiap stand pamerannya',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.black,
                      fontSize: 16,
                    ),
                    maxLines: 2,
                    softWrap: true,
                    overflow: TextOverflow.ellipsis,
                  ),
                  10.height,

                  //Button Register
                  Container(
                    height: 40,
                    padding: const EdgeInsets.fromLTRB(0, 0, 10, 0),
                    child: ElevatedButton(
                      child: const Text('Register'),
                      style: ButtonStyle(
                          backgroundColor:
                              MaterialStateProperty.all(Colors.red)),
                      onPressed: () => {
                        showDiaglogBox(context),
                        NotificationService().showNotification(
                          1, 
                          'Registrasi Berhasil', 
                          'Kamu berhasil mendaftarkan diri dalam event : STEMPreneur Day', 
                          5)
                      },
                    ).cornerRadiusWithClipRRect(10),
                  ),
                  // End Button Register
                ],
              ).expand(flex: 2),
              4.width,
              Container(
                height: 100,
                color: Colors.grey,
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 2),
                  child: Image.asset(
                    'assets/image/Acara - Innofair.jpg',
                    fit: BoxFit.cover,
                    // height: 170,
                  ),
                ),
              ).cornerRadiusWithClipRRect(16).expand(flex: 1)
            ],
          ),
          //End Event 2
        ],
      ),
    );
  }

  void showDiaglogBox(BuildContext context) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        var alertDialog = AlertDialog(
          title: const Text('Registrasi Berhasil'),
          content: const Text('Kamu telah terdaftar pada event ini'),
          actions: <Widget>[
            FlatButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: const Text('OK')),
          ],
        );
        return alertDialog;
      },
    );
  }
}
