import 'package:nextgen_event/model/source.dart';

class Event {
  Source source;
  String author;
  String title;
  String description;
  String url;
  String urlToImage;
  String publishedAt;
  String content;

  // Le's create constructor
  Event(
      {required this.source,
      required this.author,
      required this.title,
      required this.description,
      required this.url,
      required this.urlToImage,
      required this.publishedAt,
      required this.content});

  // Create the function that will map the json into a list
  factory Event.fromJson(Map<String, dynamic> json) {
    return Event(
      source : Source.fromJson(json['source']),
      author : json['author'] as String,
      title : json['title'] as String,
      description : json['descriptionr'] as String,
      url : json['authurl'] as String,
      urlToImage : json['urlToImage'] as String,
      publishedAt : json['publishedAt'] as String,
      content : json['content'] as String,
    );
  }
}
