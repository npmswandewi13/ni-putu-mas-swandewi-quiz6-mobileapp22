class Source {
  String id;
  String name;

  // Let's create constructor
  Source({required this.id, required this.name});

  // Let's create the factory function to map th json
  factory Source.fromJson(Map<String, dynamic> json) {
    return Source(id: json['id'], name: json['name']);
  }
}
