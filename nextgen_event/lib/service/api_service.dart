import 'dart:convert';

import 'package:http/http.dart';
import 'package:nextgen_event/model/Event.dart';

class ApiService {
  // Add Enpoint URL
  final endPointURL =
      "https://newsapi.org/v2/top-headlines?country=us&category=business&apiKey=eaf6591c0b6748d1bbb5d148baa1650b";
  // final endPointUrl = "https://api.nextgenapp.net/api-nextgen/event/";

  Future<List<Event>> getEvent() async {
    Response res = await get(Uri.parse(endPointURL));

    if (res.statusCode == 200) {
      Map<String, dynamic> json = jsonDecode(res.body);

      List<dynamic> body = json['event'];

      //this line will allow us to get the diffent articles from the json file and putting them into a list
      List<Event> event =
          body.map((dynamic item) => Event.fromJson(item)).toList();
      return event;
    } else {
      throw ("Can't get the Event");
    }
  }
}
